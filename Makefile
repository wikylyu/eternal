.PHONY: all eternal eworker clean

BIN=./bin

all: eternal eworker


eternal:
	go build -o $(BIN)/$@ main.go

eworker:
	go build -o $(BIN)/$@ $@/*.go


clean:
	rm -rf bin/*
